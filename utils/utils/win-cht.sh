#!/usr/bin/env bash
selected=`cat ~/utils/.win-cht-languages ~/utils/.win-cht-command | fzf`
if [[ -z $selected ]]; then
    exit 0
fi

read -p "Enter query: " query

if grep -qs "$selected" ~/utils/.win-cht-languages; then
    query=`echo $query | tr ' ' '+'`
    bash -c "echo \"curl cht.sh/$selected/$query\" & curl cht.sh/$selected/$query & while [ : ]; do sleep 1; done"
else
    bash -c "echo \"curl cht.sh/$selected~$query\" & curl -s cht.sh/$selected~$query | less -R" 
fi
