#!/usr/bin/env python3
import click
import random
import wallhavenapi
from wallhavenapi import Purity, Category, Color, Sorting


@click.command()
@click.option("--count", default=6, help="Number of wallpapers (default: 6)")
@click.option(
    "--replace", type=str, help="Replace a list of wallpapers (format: --replace 1,2,3)"
)
@click.option(
    "--keep",
    type=str,
    help="Keep select wallpapers, replace the others (format: --keep 3,4)",
)
@click.option(
    "--mode",
    type=click.Choice(["normal", "anime", "fun"], case_sensitive=False),
    help="The kind of wallpapers to get",
    required=True,
)
def get_wallpapers(count, replace, keep, mode):

    wallhaven_api = wallhavenapi.WallhavenApiV1(
        api_key="Xfc0cm8Gf7UMHLGuLLaCFLTXL4Az6IxV"
    )

    results = {}
    path = "default"
    match mode:
        case "normal":
            results = wallhaven_api.search(
                q="",
                purities=[Purity.sfw],
                categories=[Category.general],
                ratios=[(16, 9), (16, 10)],
                colors=random.choice(
                    [
                        Color.pacific_blue,
                        Color.gun_powder,
                        Color.downy,
                        Color.dusty_gray,
                        Color.silver,
                    ]
                ),
                sorting=Sorting.toplist,
            )
            path = "default"
        case "anime":
            results = wallhaven_api.search(
                q="",
                purities=[Purity.sfw],
                categories=[Category.anime],
                ratios=[(16, 9), (16, 10)],
                sorting=Sorting.toplist,
            )
            path = "fun"
        case "fun":
            # add search terms, use -term to exclude
            search_terms = [
                # add search terms here
            ]
            results = wallhaven_api.search(
                q=" ".join(search_terms),
                purities=[Purity.sketchy],
                categories=[Category.people],
                ratios=[(16, 9), (16, 10)],
                sorting=Sorting.random,
            )
            path = "fun"

    picks = random.sample(range(0, len(results["data"])), count)
    if replace:
        rep = [int(i) for i in replace.split(",")]
        box = dict(zip(rep, picks[0 : len(rep)]))
        for i, p in box.items():
            get = results["data"][p]["id"]
            wallhaven_api.download_wallpaper(
                wallpaper_id=get,
                file_path=f"/home/casey/.wallpapers/images/{path}/{i}.png",
            )
    elif keep:
        rep = [i for i in range(0, count) if i not in [int(j) for j in keep.split(",")]]
        box = dict(zip(rep, picks[0 : len(rep)]))
        for i, p in box.items():
            get = results["data"][p]["id"]
            wallhaven_api.download_wallpaper(
                wallpaper_id=get,
                file_path=f"/home/casey/.wallpapers/images/{path}/{i}.png",
            )
    else:
        for i, p in enumerate(picks):
            get = results["data"][p]["id"]
            wallhaven_api.download_wallpaper(
                wallpaper_id=get,
                file_path=f"/home/casey/.wallpapers/images/{path}/{i}.png",
            )


if __name__ == "__main__":
    get_wallpapers()
