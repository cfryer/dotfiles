#!/bin/sh
pkill -9 -O 10 set-wallpaper
DE=$DESKTOP_SESSION

while getopts m: flag
do
    case "${flag}" in
        m) mode=${OPTARG};;
    esac
done
rm -rf $HOME/.config/wpg/schemes
mkdir $HOME/.config/wpg/schemes
if [ "$mode" = "fun" ]; then
    DIR="fun"
else
    DIR="default"
    rm -rf $HOME/.wallpapers/images/fun
    mkdir $HOME/.wallpapers/images/fun
    find $HOME/.config/wpg/wallpapers -xtype l -delete
fi
while true; do
    MINUTE=$(date -u +%M)
    PICK=${MINUTE:0:1}
    PID=`pgrep swaybg`
    PICKWP=$HOME/.wallpapers/images/${DIR}/${PICK}.png
    wpg -d ${PICK}.png &> /dev/null
    wpg -a ${PICKWP} &> /dev/null
    wpg -ns ${PICK}.png &> /dev/null
    swaybg -i ${PICKWP} -m fill &> /dev/null &
    sleep 1
    kill -9 $PID
    sleep 599
done &> /dev/null
