# Git Repository for dotfiles

Use GNU/Stow to symlink the files into the correct locations.

1. cd into dotfiles directory
2. use ```stow <name of program```
    - example:  stow nvim
