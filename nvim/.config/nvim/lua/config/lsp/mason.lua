local mason_status_ok, mason = pcall(require, "mason") 
if not mason_status_ok then
    return
end

local masonlsp_status_ok, masonlsp = pcall(require, "mason-lspconfig")
if not masonlsp_status_ok then
    return
end

mason.setup {}
masonlsp.setup {
    ensure_installed = {"sumneko_lua","gopls"}
}

local lspconfig_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_ok then
    return
end

lspconfig.bashls.setup {}
lspconfig.gopls.setup{}
lspconfig.html.setup{}
lspconfig.jsonls.setup{require('config.lsp.settings.jsonls')}
lspconfig.sumneko_lua.setup{require('config.lsp.settings.sumneko_lua')}
lspconfig.pyright.setup{require('config.lsp.settings.pyright')}
lspconfig.pylsp.setup{}
lspconfig.rome.setup{}
lspconfig.sqls.setup{}
lspconfig.tsserver.setup{}
lspconfig.yamlls.setup{}
