-- Choose one of:
--   1. dracula
--   2. gruvbox
--   3. monokai_pro
--   4. OceanicNext

local pick_color = 'OceanicNext'

function set_color (color)
    if color == 'gruvbox' then
        vim.cmd([[set background=dark]])
        vim.cmd(string.format([[colorscheme %s]], color))
    else
        vim.cmd(string.format([[colorscheme %s]], color))
    end
end

set_color(pick_color)
