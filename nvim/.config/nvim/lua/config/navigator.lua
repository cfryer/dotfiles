local status_ok, navigator = pcall(require, "Navigator")
if not status_ok then
	return
end

navigator.setup()
local map = vim.api.nvim_set_keymap
opts = {noremap = true, silent = true}
map("n", "<C-h>", "<cmd>lua require('Navigator').left()<CR>", opts)
map("n", "<C-k>", "<cmd>lua require('Navigator').up()<CR>", opts)
map("n", "<C-l>", "<cmd>lua require('Navigator').right()<CR>", opts)
map("n", "<C-j>", "<cmd>lua require('Navigator').down()<CR>", opts)
