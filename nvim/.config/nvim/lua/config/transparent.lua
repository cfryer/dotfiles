require("transparent").setup({
    enable = true,
    extra_groups = {
        "BufferLineTabClose",
        "BufferLineBufferSelected",
        "BufferLineFill",
        "BufferLineBackground",
        "BufferLineSeparator",
        "BufferLineIndicatorSelected",
    },
    exclude = {}
})
