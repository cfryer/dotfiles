status_ok, toggleterm = pcall(require, "toggleterm")
if not status_ok then
    return
end

toggleterm.setup {
    function(term)
        if term.direction == "horizontal" then
            return 15
        elseif term.direction == "vertical" then
            return vim.o.columns * 0.4
        end
    end,
    open_mapping = [[<c-\>]],
    hide_numbers = true,
    shade_filetypes = {},
    shade_terminals = true,
    shading_factor = 0.8,
    start_in_insert = true,
    insert_mappings = true,
    persist_size = true,
    direction = 'float',
    close_on_exit = true,
    shell = vim.o.shell,
    float_opts = {
        border = 'single',
        width = 120,
        height = 40,
        winblend = 3,
        highlights = {
            border = "Normal",
            background = "Normal"
        }
    }
}

local opts = { noremap = true, silent = true }
local Terminal = require('toggleterm.terminal').Terminal

local cht_sh = Terminal:new({ cmd = "sh ~/utils/win-cht.sh", hidden = true})
function _cht_sh_toggle()
    cht_sh:toggle()
end

vim.api.nvim_set_keymap("n", "<leader>qq", "<cmd>lua _cht_sh_toggle()<CR>", opts)

vim.api.nvim_set_keymap("n", "<leader>~", ":ToggleTerm dir=git_dir<CR>", opts)
