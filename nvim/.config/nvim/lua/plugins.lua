local fn = vim.fn

-- returns the require statement for use in `config` parameter of
-- packer's use fucntion. (expects the name of config file)
function get_config(name)
	return string.format("require(\"config/%s\")",name)
end

function set_color(name)
    return string.format("vim.cmd[[colorscheme %s]]", name)
end

local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

-- install packer if not already installed
if fn.empty(fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
    }
    print("Installing packer. Close and reopen Neovim.")
    vim.cmd [[packadd packer.nvim]]
end

-- Autocommand to reload neovim whenever you save the plugins.lua file
vim.cmd [[
    augroup packer_user_config
        autocmd!
        autocmd BufWritePost plugins.lua source <afile> | PackerSync
    augroup end
]]

-- initialize and configure packer
local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float {border = "rounded"}
        end,
    },
}

return packer.startup(function(use)
    -- Plugins List

    -- Plugin management
    use "wbthomason/packer.nvim"

    -- Color scheme
    use {
        {"Mofiqul/dracula.nvim", }, -- dracula
        {"gruvbox-community/gruvbox", }, -- gruvbox
        {"phanviet/vim-monokai-pro", }, -- monokai_pro
        {"mhartington/oceanic-next", }, -- OceanicNext
    }

    -- Fancy Icons
    use {"kyazdani42/nvim-web-devicons"}

    -- Telescope and other OP things
    use {
        "nvim-telescope/telescope.nvim",
        requires = {{"nvim-lua/popup.nvim"}, {"nvim-lua/plenary.nvim"}},
        config = get_config("telescope")
    }
    use {"nvim-telescope/telescope-fzf-native.nvim", run = "make"}
    use {
        "kevinhwang91/nvim-bqf",
        requires = {{"junegunn/fzf", module = "nvim-bqf"}}
    }
    use {
        "Shatur/neovim-session-manager",
        requires = {
            {"nvim-telescope/telescope.nvim"},
            {"nvim-telescope/telescope-ui-select.nvim"}
        },
        config = get_config("session-manager")
    }

    -- File Navigation
    use {"kyazdani42/nvim-tree.lua", config = get_config("nvim-tree")}
    use {"ThePrimeagen/harpoon"}

    -- QOL Improvements
    use {"numToStr/Navigator.nvim", config = get_config("navigator")}
    use {
        "nvim-lualine/lualine.nvim",
        config = get_config("lualine"),
        event = "VimEnter",
        requires = {"kyazdani42/nvim-web-devicons", opt = true}
    }
    use {
        "norcalli/nvim-colorizer.lua",
        event = "BufReadPre",
        config = get_config("colorizer")
    }
    use {
        "numToStr/Comment.nvim",
        opt = true,
        keys = {"gc", "gcc"},
        config = get_config("comment"),
        requires = {"joosepAlviste/nvim-ts-context-commentstring"}
    }
    use {
        'goolord/alpha-nvim',
        requires = { 'kyazdani42/nvim-web-devicons' },
        config = get_config("alpha")
    }

    -- Treesitter and similar context utilities
    use {
        "nvim-treesitter/nvim-treesitter",
        config = get_config("treesitter"),
        run = ":TSUpdate"
    }
    use {"windwp/nvim-autopairs", config = get_config("autopairs")}
    use {"windwp/nvim-ts-autotag", config = get_config("autotag")}

    -- Completion Engine
    use {
        "hrsh7th/nvim-cmp",
        requires = {
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "saadparwaiz1/cmp_luasnip",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-emoji",
            "hrsh7th/cmp-nvim-lua",
        },
        config = get_config("cmp")
    }

    -- Snippet Engine
    use {
        "L3MON4D3/LuaSnip",
        requires = {
            "rafamadriz/friendly-snippets" -- curated snippets
        }
    }

    -- LSP
    use {
        "neovim/nvim-lspconfig",
        requires = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "tamago324/nlsp-settings.nvim",
            "jose-elias-alvarez/null-ls.nvim",
            "simrat39/symbols-outline.nvim",
            "ray-x/lsp_signature.nvim",
            "b0o/SchemaStore.nvim",
            "glepnir/lspsaga.nvim",
            "simrat39/rust-tools.nvim"
        },
        config = get_config("lsp") -- this one is a directory
    }

    use { "filipdutescu/renamer.nvim", config = get_config("renamer") }
    use {
        "folke/trouble.nvim",
        cmd = "TroubleToggle"
    }

    -- Keybind manager
    use {
        "folke/which-key.nvim",
        event = "VimEnter",
        config = get_config("which-key")
    }

    -- Git
    use {
        "TimUntersberger/neogit",
        requires = {"nvim-lua/plenary.nvim","sindrets/diffview.nvim"},
        config = get_config("neogit")
    }
    use {
        "lewis6991/gitsigns.nvim",
        config = get_config("gitsigns")
    }

    -- Nvim Terminal
    use {"akinsho/toggleterm.nvim", config = get_config("toggleterm")}

    -- Nvim Transparency
    use {"xiyaowong/nvim-transparent", config = get_config("transparent")}

    -- vim support for yuck (https://elkowar.github.io/eww/)
    use {"https://github.com/elkowar/yuck.vim"}
    use {"gpanders/nvim-parinfer"}

    if PACKER_BOOTSTRAP then
        require("packer").sync()
    end
end)
