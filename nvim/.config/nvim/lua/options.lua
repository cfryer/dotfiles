local o = vim.opt
local wo = vim.wo
local fn = vim.fn

vim.cmd "filetype indent plugin on"
vim.cmd "set inccommand=split"

o.hidden = true
o.exrc = true
o.signcolumn = "yes:2"
o.wrap = false
o.mouse = "a"
o.confirm = true
o.updatetime = 300
o.timeoutlen = 400
o.backup = false
o.swapfile = false
o.undofile = true
o.undodir = fn.stdpath("data") .. "/undodir"
o.history = 500
o.clipboard = "unnamedplus"
o.fileencoding = "utf-8"
o.conceallevel = 0
o.relativenumber = true
o.number = true
o.cmdheight = 1
o.showmode = false
o.showtabline = 2
o.laststatus = 2
o.smartcase = true
o.smartindent = true
o.splitbelow = true
o.splitright = true
o.expandtab = true
o.shiftwidth = 4
o.tabstop = 4
o.termguicolors = true
o.cursorline = true
o.scrolloff = 8
o.sidescrolloff = 8
o.ignorecase = true
o.foldenable = false
o.foldmethod = "expr"
o.foldexpr = "nvim_treesitter#foldexpr()"
o.listchars = "tab:▸\\,trail:·"
o.shortmess = o.shortmess + "c"
wo.colorcolumn = "80"
o.wildmode = "longest:full,full"
o.lazyredraw = true
