-- Plugin management via Packer
require ("plugins") -- Plugins generally require a config file located at /lua/config/
-- Key mappings, see lua/config/which.lua
require ("mappings")
-- Standard vim options
require ("options")
-- Vim autocommands/autogroups
require ("autocmd")

require ("config.colorscheme")
